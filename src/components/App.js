import React, { Component } from 'react';
import PrimaryMenu from './PrimaryMenu';
import Footer from './Footer';
import Homepage from './Homepage';
import Sidebar from './Sidebar';
import SearchPage from './SearchPage';

const App = (props) => {
  return(
    <div>
      <PrimaryMenu/>
      <SearchPage />
      <Footer/>
    </div>
 )
}
export default App;
