import React, { Component } from 'react';

const PrimaryMenu = (props) => {
  return(
    <header className="stickynav z8" role="banner">
      <nav role="navigator">
        <section className="d-sm-h d-lg-b">
          <div className="ncss-row border-bottom-light-grey">
            <div className="ncss-col-sm-12 prl0-sm">
              <ul className="fl-sm-l d-sm-ib">
                <li className="member-nav-item border-right-light-grey d-sm-ib">
                  <a href="#" className="hover-color-black prl7-sm pt2-sm pb2-sm text-color-grey fs12-sm d-sm-b u-uppercase" aria-label="Nike HR Website">Nike HR</a>
                </li>
                <li className="member-nav-item border-right-light-grey d-sm-ib">
                  <a href="#" className="hover-color-black prl7-sm pt2-sm pb2-sm text-color-grey fs12-sm d-sm-b u-uppercase" aria-label="Zero Website">Zero</a>
                </li>
              </ul>
              <ul className="member-nav-right fl-sm-r d-sm-ib pr5-sm">
                <li className="js-login-cta member-nav-item d-sm-ib va-sm-m">
                  <a href="#" className="js-login-btn hover-color-black text-color-grey prl3-sm pt2-sm pb2-sm fs12-sm d-sm-b" aria-label="Join / Log in">Log In To NikeU</a>
                </li>
                <li className="js-my-account member-nav-item d-sm-ib d-sm-h has-submenu va-sm-m">
                  <a href="#" className="hover-color-black text-color-grey prl3-sm pt2-sm pb2-sm fs12-sm d-sm-b" aria-label="My Account">
                    <i className="g72-profile fs16-sm mr2-sm"></i> Nestor Navidad
                    <i className="g72-arrow-thick-down ml2-sm"></i>
                  </a>
                  <ul className="member-nav-item-dropdown bg-white border-light-grey prl7-sm pt5-sm pb5-sm z4">
                    <li>
                      <a href="/user/4638/info" className="fs12-sm hover-color-grey">My Info</a>
                    </li>
                    <li>
                      <a href="#" className="js-login-btn fs12-sm hover-color-grey">Log Out</a>
                    </li>
                  </ul>
                </li>
                <li className="member-nav-item d-sm-ib va-sm-m">
                  <i className="g72-globe pr1-sm"></i>
                  <select id="lang-dropdown">
                    <option value="en">English</option>
                    <option value="de">German</option>
                    <option value="es">Spanish</option>
                    <option value="fr">French</option>
                  </select>
                </li>
              </ul>
            </div>
          </div>
          <div className="ncss-row border-bottom-light-grey u-position-rel">
            <div className="ncss-col-lg-1 ncss-col-xl-3 va-sm-m">
              <h1>
                <a href="/" className="prl4-sm pt4-sm pb4-sm d-sm-ib fs20-lg fs24-xl">
                  <i className="g72-swoosh pr2-sm d-sm-h d-xl-ib"></i>
                  <span className="ncss-marketing u-italic u-uppercase">Search</span>
                </a>
              </h1>
            </div>
            <div className="ncss-col-lg-8 ncss-col-xl-6 u-position-st va-sm-m">
              <ul className="ta-sm-c">
                <li className="primary-nav-item d-sm-ib">
                  <a className="ncss-brand fs16-sm prl4-sm pt6-sm pb6-sm u-uppercase d-sm-ib">Schedule</a>
                </li>
                <li className="primary-nav-item d-sm-ib">
                  <a className="ncss-brand fs16-sm prl4-sm pt6-sm pb6-sm u-uppercase d-sm-ib">Professional Skills</a>
                </li>
                <li className="primary-nav-item d-sm-ib">
                  <a className="ncss-brand fs16-sm prl4-sm pt6-sm pb6-sm u-uppercase d-sm-ib">SFL 2.0</a>
                </li>
                <li className="primary-nav-item d-sm-ib">
                  <a className="ncss-brand fs16-sm prl4-sm pt6-sm pb6-sm u-uppercase d-sm-ib">Winning Habits</a>
                </li>
                <li className="primary-nav-item d-sm-ib">
                  <a className="ncss-brand fs16-sm prl4-sm pt6-sm pb6-sm u-uppercase d-sm-ib">Help</a>
                </li>
              </ul>
            </div>
            <div className="ncss-col-lg-3 pr8-sm va-sm-m">
              <form action="" method="get" id="views-exposed-form-site-search-site-search" acceptCharset="UTF-8">
                <div className="js-form-item form-item js-form-type-textfield form-type-textfield js-form-item-keyword form-item-keyword">
                    <div className="js-global-search-container ncss-form-group global-search-container">
                      <div className="mod-ncss-input-container ncss-input-container">
                        <input type="text" name="keyword" id="search" className="js-search-input global-search-input ncss-input pt2-sm pr4-sm pb2-sm pl10-sm u-rounded d-sm-ib" placeholder="Search"/>
                          <button className="js-btn-clear-input btn-clear-input z2 bg-transparent d-sm-h"><i className="g72-x-small fs20-sm text-color-grey"></i></button>
                          <button className="btn-search z2 bg-transparent"><i className="g72-search fs20-sm"></i></button>
                      </div>
                    </div>
                  </div>
              </form>
            </div>
          </div>
        </section>
      </nav>
    </header>
  )
}
export default PrimaryMenu;
