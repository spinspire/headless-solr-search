import React, { Component } from 'react';

export default class Facet extends React.Component {

  printFacet(data){
    const items = [];
    data.forEach((item,index) => {
      if(index % 2 == 0) {
        if(data[index+1] != 0) {
          const machineName = this.props.drupalField.replace(/\s/,'_').toLowerCase();
          const itemMachineName = item.replace(/[^A-Za-z0-9 ]/g,'').replace(/\s{2,}/g,' ').replace(/\s/g, "-");
          if(this.props.solrField == 'hash'){
            const sitename = this.hastToSite(item);
            items.push(<li key={index} className="facet-item"><input type="checkbox" className="facets-checkbox" name={this.props.drupalField.toLowerCase()} id={`${machineName + '-' + itemMachineName}`}/><label htmlFor={`${machineName + '-' + item}`}><span>{sitename}</span> <span>({data[index+1]})</span></label></li>)
          } else {
            items.push(<li key={index} className="facet-item"><input type="checkbox" className="facets-checkbox" name={this.props.drupalField.toLowerCase()} id={`${machineName + '-' + itemMachineName}`}/><label htmlFor={`${machineName + '-' + item}`}><span>{item}</span> <span>({data[index+1]})</span></label></li>)
          }
        }
      }
    })
    return items;
  }

  hastToSite(hash){
    switch(hash){
      case '96yl4e':
        return 'site1';
        break;
      case 'jskgq3':
        return 'site2';
        break;
      default:
        return 'unknown';
        break;
    }
  }

  render() {
    return(
      <div id={`block-${this.props.drupalField.replace(/\s/,'').toLowerCase()}`} className={`filter-${this.props.drupalField.replace(/\s/,'-').toLowerCase()} prl5-sm pt8-lg mb4-sm block-facet--checkbox block block-facets`}>
        <h4 className="ncss-brand fs16-sm fs18-lg u-uppercase mb3-lg">{this.props.drupalField}</h4>
        <ul className={`filter-${this.props.drupalField.replace(/\s/,'-').toLowerCase()} mb8-lg js-facets-checkbox-links item-list__checkbox default-query-coder`}>
          { Object.keys(this.props.data).length > 0 ? this.printFacet(this.props.data[this.props.solrField]) : '' }
        </ul>
      </div>
    )
  }
}
