import React, { Component } from 'react';

const Footer = (props) => {
  const svg = '<svg className="icon shape-nike-logos" viewBox="0 0 351.4 35.7"> <use xlink:href="#nike4logos" xmlns:xlink="http://www.w3.org/1999/xlink"></use></svg>';
  return(
    <footer className="footer bg-offwhite border-top-light-grey" role="contentinfo">
      <div className="region region-footer">
        <div className="ncss-container">
          <div className="ncss-row prl8-sm pt4-sm pb4-sm">
            <div className="footer-left ncss-col-sm-12 ncss-col-lg-5 ta-sm-c ta-lg-l va-lg-m">
              <small className="text-color-grey">
                <a className="text-color-grey d-sm-b d-lg-ib" href="#" id="toolbar-tab-tour">Site Tour</a> 
                <span className="d-sm-h d-lg-ib prl2-sm">|</span>
                <a className="text-color-grey d-sm-b d-lg-ib" href="/faq">FAQ</a>
                <span className="d-sm-h d-lg-ib prl2-sm">|</span>
                <a className="text-color-grey d-sm-b d-lg-ib" href="https://nikehr-global.custhelp.com/" target="_blank">Report an issue</a>
              </small>
            </div>
            <div className="ncss-col-sm-12 ncss-col-lg-2 ta-sm-c pt3-sm pb2-sm pt1-lg pb0-lg va-lg-m" dangerouslySetInnerHTML={{ __html: svg }} />
            <div className="footer-right ncss-col-sm-12 ncss-col-lg-5 ta-sm-c ta-lg-r va-lg-m">
              <p className="copyright text-color-grey u-sm-b d-lg-ib"><small>© 2018 Nike, Inc. All Rights Reserved </small></p>
            </div>
          </div>
        </div>
      </div> 
    </footer>
  )
}
export default Footer;
