import React, { Component } from 'react';
import Sidebar from './Sidebar';
import SearchResult from './SearchResult';

export default class SearchPage extends Component {

  constructor(props){
    super(props);
    this.state = {
      searchResults: {
        total: '',
        results: [],
        facetData: {}
      },
      lang: localStorage.getItem('lang') || 'en',
      activeFacets: {},
      filterQuery: ''
    };
    this.setActiveFilters = this.setActiveFilters.bind(this);
  }

  componentDidMount(){
    const langDropdown = document.getElementById('lang-dropdown')
    langDropdown.addEventListener('change',function() {
      localStorage.setItem('lang',this.value);
    })
    const index = langDropdown.querySelector('#lang-dropdown option[value="' + this.state.lang+ '"]').index;
    langDropdown.selectedIndex = index;
    const query = document.getElementById('search');
    let keepWaiting = false;
    // Logic for autocomplete.
    query.addEventListener('keypress', () => {
      // We don't want the logic to fire on each keypress, so we just return until the ajax request has been completed.
      if(keepWaiting){
        return
      }
      setTimeout(function() {
        const lang = localStorage.getItem('lang');
        fetch(`http://solr.server8.spinspire.com/solr/site2/select?omitHeader=true&wt=json&json.nl=flat&start=0&rows=10&fl=tm_X3b_${lang}_title%2Cscore&sort=score+desc&fq=ss_search_api_language:"${lang}"&q=tm_X3b_${lang}_title:${query.value}`, {
          headers: {}
        }).then(response => {
          return response.json();
        }).then(results => {
          // Create HTML elements which will contain the autocomplete suggestions.
          var node = document.createElement("div");
          node.id = 'autocomplete-suggestions';
          results.response.docs.forEach(function(item,index) {
            let option = document.createElement("div");
            option.className = 'option';
            const t = document.createTextNode(item['tm_X3b_'+lang+'_title'][0]);
            option.appendChild(t);
            // If an option is chosen update the value in the search field and submit the form.
            option.addEventListener('click',function(){
              document.getElementById('search').value = option.innerHTML;
              document.getElementsByClassName('btn-search')[0].click();
            })
            node.appendChild(option);
          })
          query.parentNode.appendChild(node);
          keepWaiting = false;
        })
      },750);
      keepWaiting = true;
    })
    // Add listener to remove the suggestions if user clicks outside of it.
    document.body.addEventListener('click',function(){
      if(document.getElementById('autocomplete-suggestions')) {
        document.getElementById('autocomplete-suggestions').remove();
      }
    })
  }

  setActiveFilters(facet_url) {
    let {activeFacets,filterQuery} = this.state;
    let params = facet_url.split('&');
    if(params[0] == "" || params[0] === 'null')
      params.shift();
    if(params.length != 0) {
      if(params[0].charAt(0) == '?')
        params[0] = params[0].substring(1,params[0].length);
      params.map((param,index) => {
        const parts = param.split('=');
        const paramName = this.getSolrField(parts[0]);
        const paramValue = decodeURIComponent(parts[1].replace(/-/g,' '));
        // Multivalued filters
        if(paramValue.indexOf(',') > 0) {
          const values = paramValue.split(',')
          activeFacets[parts[0]] = [];
          let concatParam = '';
          // Concatenate the paramerters into one string.
          values.forEach(function(value) {
            concatParam += paramName + '"' + value + '" ';
          })
          activeFacets[parts[0]].push('&fq=(' + encodeURIComponent(concatParam) + ')');
        } else {
          activeFacets[parts[0]] = [];
          if(parts[0] == 'keyword') {
            if(parts[1] == "") {
              activeFacets[parts[0]].push('&q=*:*');
            } else {
              activeFacets[parts[0]].push('&q=' + paramName + paramValue + '^5%20OR%20tm_X3b_' + this.state.lang + '_body:' + paramValue + '^2');
              //activeFacets[parts[0]].push('&q=' + paramName + paramValue + '^3');
            }
          } else {
            activeFacets[parts[0]].push('&fq=' + paramName + '"' + encodeURIComponent(paramValue) + '"');
          }
        }
      });
    }
    if(!activeFacets.hasOwnProperty('keyword')) {
      activeFacets.keyword = [];
      activeFacets.keyword.push('&q=*:*');
    }
    filterQuery = this.flattenQuery(activeFacets);
    // We set the state and then wait for the action to resolve before executing out callback function.
    // https://stackoverflow.com/a/29490883
    this.setState({
      activeFacets,
      filterQuery
    }, this.search);
  }

  search() {
    const {filterQuery} = this.state;
    let searchResults = [];
    var searchQuery = document.getElementById('search');
    const baseQuery = `http://solr.server8.spinspire.com/solr/site2/select?omitHeader=true&wt=json&json.nl=flat&start=0&rows=100&fl=%2A%2Cscore&sort=score+desc&facet.field=%7B%21key=sm_term_name%20ex=facet%3Aterm_name%7Dsm_term_name&facet.field=%7B%21key=ss_type%20ex=facet%3Atype%7Dss_type&&facet.field=%7B%21key=hash%20ex=facet:type%7Dhash&facet=on&fq=ss_search_api_language:"${this.state.lang}"`;
    fetch(baseQuery + filterQuery,{
      headers: {}
    }).then(response => {
      return response.json();
    }).then(results => {
      console.log(results);
      results.response.docs.map((result,index)=> {
        const key = Math.floor(Math.random() * Math.floor(99999));
        searchResults.push(<SearchResult key={key} data={result} />);
      })
      this.setState({
        searchResults: {
          total: results.response.numFound,
          results: searchResults,
          facetData: results.facet_counts.facet_fields,
        }
      })
    })
  }

  flattenQuery(facets){
    let finalQuery = '';
    Object.values(facets).forEach(function(params){
      if(params.length == 1){
        finalQuery += params[0];
      } else {
        params.forEach(function(param,i) {
          finalQuery += param;
        });
      }
    });
    return finalQuery;
  }

  getSolrField(field){
    let name = '';
    switch(field) {
      case 'content_type':
        name = 'ss_type:';
        break;
      case 'term':
        name = 'sm_term_name:';
        break;
      case 'keyword':
        name = 'tm_X3b_' + this.state.lang + '_title:';
        break;
      case 'site':
        name = 'hash:';
        break;
      default:
        break;
    }
    return name;
  }

  render() {
    const {searchResults} = this.state;
      return(
        <div className="ncss-container mt6-sm mb6-sm mt8-lg mb8-lg pb4-sm pb6-lg">
          <Sidebar setFilters={this.setActiveFilters} facetData={this.state.searchResults.facetData}/>
          <div className="ncss-col-sm-12 ncss-col-lg-9 ncss-col-xl-10 va-sm-t border-left-light-grey">
            <section>
              { this.state.searchResults.results.length > 0 ? (<React.Fragment><h4 className="ncss-brand fs24-sm mb8-sm">Number of Results: ({this.state.searchResults.total})</h4>{this.state.searchResults.results}</React.Fragment>) : 'Loading...' }
            </section>
          </div>
        </div>
      )
  }
}
