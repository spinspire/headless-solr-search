import React, { Component } from 'react';

const SearchResult = (props) => {
  return (
    <div className="list-link-row ncss-row border-light-grey ml0-sm mr0-sm prl2-sm pt4-sm pb4-sm">
      <div className="ncss-col-sm-12 ncss-col-lg-10 va-sm-m">
        <div className="d-sm-b d-lg-t">
          <div className="d-sm-ib d-lg-tc pr3-lg va-sm-t va-lg-m ta-sm-c">
            <div className="list-icon icon-link">
              <p className="">[icon]</p>
              <p className="ncss-brand u-uppercase text-color-dark-grey d-sm-h d-lg-b">{props.data.ss_type}</p>
            </div>
          </div>
          <div className="ncss-col-sm-10 ncss-col-lg-12 d-sm-ib d-lg-b">
            <h4 className="ncss-brand u-uppercase fs16-sm fs18-lg"><a href={props.data.site + props.data.sort_url}>{props.data['tm_X3b_' + props.data.ss_search_api_language + '_title']}</a></h4>
          </div>
          <div className="list-data text-color-grey ncss-col-lg-12 d-sm-ib d-lg-b va-sm-m pt1-sm pt0-lg field--name-body">
            {props.data.tm_X3b_en_body ? <p className="text-color-grey">{props.data['tm_X3b_' + props.data.ss_search_api_language + '_body'][0].substring(0,250)}</p> : ''}
            <div className="ncss-row pt2-sm pt0-lg">
              <div className="duration ncss-col-sm-12 ncss-col-md-3 va-sm-m">
                <p className="text-color-grey"><strong>Site:</strong> {props.data.hash}</p>
              </div>
              <div className="duration ncss-col-sm-12 ncss-col-md-3 va-sm-m">
                <p className="text-color-grey"><strong>Score:</strong> {props.data.score}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SearchResult;
