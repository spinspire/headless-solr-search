import React, { Component } from 'react';
import Facet from './Facet';

export default class Sidebar extends React.Component {

  constructor(props){
    super(props);
  }

  componentDidMount(){
    document.getElementById('submit-filters').addEventListener('click',() => {this.buildUrl()});
    // Fires intiial search
    this.props.setFilters(window.location.search);
  }

  initializeFacets(){
    const query = window.location.search;
    if(query.indexOf('keyword') > 0) {
      let keyword = window.location.search.split('&');
      keyword = keyword[0].split('=').pop();
      document.getElementById('search').value = keyword.replace(/[^A-Za-z ]/g,' ').trim().replace(/\s{2,}/g,' ');
    }
    let params;
    if(query.indexOf('keyword') > 0) {
      params = query.split('&');
    } else {
      params = query.split(/\?|&/);
    }
    params.shift();
    params.forEach((param,index) => {
      if(param == "") return;
      const parts = param.split('=');
      const paramName = parts[0].replace('_','');
      const paramValue = decodeURIComponent(parts[1].replace(/-/g,' '));
      let block = document.getElementById('block-' + paramName);
      let options = block.querySelectorAll('li.facet-item');
      for(var i = 0; i < options.length; i++) {
        if(paramValue.indexOf(',') > 0) {
          const values = paramValue.split(',')
          values.forEach(function(value) {
            let tmpName = options[i].lastChild.htmlFor.replace(/.*-/,'').toLowerCase();
            if(tmpName == value)
              options[i].firstChild.checked = true;
          })
        } else {
          let tmpName = options[i].lastChild.htmlFor.replace(/.*-/,'').toLowerCase();
          if(tmpName == paramValue) {
            options[i].firstChild.checked = true;
          }
        }
      }
    })
  }

  buildUrl() {
    let filter_prefix;
    let filter_name;
    let all_filters = '';
    let new_url;
    let keyword = document.getElementById('search').value != "" ? '?keyword=' + document.getElementById('search').value : null;;
    //let keyword = filters[0].split('=').pop();
    const base_url = window.location.origin;
    let screen_size;
    // Creates a variable based on the screen size so that desktop and mobile filters are not selected at the same time
    if (window.innerWidth < 1024) {
      screen_size = 'div.js-filter-modal';
    } else {
      screen_size = 'aside.filter-col';
    }
     const items = document.querySelectorAll(screen_size + ' input:checked');
     // Loops through all the checked checkboxes and forms the url
     for(var i = 0; i < items.length; i++) {
       var html =  items[i].parentElement.parentElement.previousSibling.innerHTML;
       switch(html) {
         case 'Content Type':
           all_filters = this.buildFilterUrl(items[i],all_filters);
           break;
         case 'Site':
           all_filters = this.buildFilterUrl(items[i],all_filters);
           break;
         case 'Term':
           all_filters = this.buildFilterUrl(items[i],all_filters);
           break;
         case 'Functional':
           all_filters = this.buildFilterUrl(items[i],all_filters);
           break;
         default:
         break;
       }
     }
     if(keyword == "" || keyword === null) {
        if(all_filters.charAt(0) == '&')
          all_filters = all_filters.substring(1,all_filters.length)
        new_url = base_url + '?' + all_filters;
     } else if(all_filters.charAt(0) !== '&') {
       if(all_filters == '')
         new_url = base_url + keyword;
       else
        new_url = base_url + keyword + '&' + all_filters;
     } else {
        new_url = base_url + keyword + all_filters;
     }
     //this.props.setFilters(keyword + all_filters);
     location.href = new_url;
  }

  // Helper function to build facet based urls.
  buildFilterUrl(input,all_filters) {
    let filter_name = input.nextSibling.htmlFor;
    let filter_prefix = filter_name.replace(/-.*/,'');
    if(all_filters.includes(filter_prefix)) {
      // Split the parameters out into an array.
      let filters = all_filters.split('&');
      if(filters[0] == '') filters.shift();
      filters.forEach(function(item,index){
        // If current value being procesed belongs to an exisiting value in the query string.
        if(item.includes(filter_prefix)) {
          let split_items = item.split('=');
          // remove name of parameter
          split_items.shift();
          // recomine after removing and desired index.
          item = split_items[0];
          let multiValued = input.parentElement.parentElement.classList.value.includes('taxonomy-query-coder');
          // Split on comma.
          let parts = item.split('%2C');
          // Get the name of the options we're adding to the url.
          let new_name = input.nextSibling.htmlFor.replace(/.*-/,'');
          new_name = new_name.replace(/\s/g,'-').toLowerCase();
          parts.push(new_name);
          let new_filter = parts.join(',');
          // encode url to ensure proper url structure
          let encoded = encodeURIComponent(new_filter);
          let filter = filter_prefix + '=' + encoded;
          // remove 'old' parameter and replace it with new one with the updated values.
          filters.splice(index,1,filter);
          all_filters = filters.join('&');
        }
      });
    } else {
      var name = input.nextSibling.htmlFor.replace(/.*-/,'');
      name = name.replace(/\s/g,'-').toLowerCase();
      var filter = filter_prefix + '=' + encodeURIComponent(name);
      all_filters = all_filters + '&' + filter;
    }
    return all_filters;
  }

  resetFilters(){
    const query = window.location.search
    if(query.includes('keyword')) {
      let keyword = query.split('&');
      window.location.href = window.location.origin + keyword[0];
    } else {
      window.location.href = window.location.origin;
    }
  }

  render() {
    if(Object.keys(this.props.facetData).length > 0) {
      console.log(this.props.facetData);
      const that = this;
      // A slight delay is required so that the elements render before we start looking for them.
      setTimeout(function() {
        that.initializeFacets();
      },50);
    }
    return(
      <aside className="filter-col ncss-col-lg-3 ncss-col-xl-2" role="sidebar">
        <div className="region region-sidebar">
          <Facet data={this.props.facetData} solrField="ss_type" drupalField="Content Type" />
          <Facet data={this.props.facetData} solrField="hash" drupalField="Site" />
          <Facet data={this.props.facetData} solrField="sm_term_name" drupalField="Term" />
          <div id="block-searchfilterbuttons2" className="ncss-row block block-block-content block-block-content7b00981d-9366-4dfb-84e4-705e5ef4f597">
            <div className="clearfix text-formatted field field--name-body field--type-text-with-summary field--label-hidden field__item"><div className="ncss-col-sm-6 mb4-sm">
              <div className="ncss-btn-border-black ncss-brand pt2-sm pb2-sm u-uppercase u-full-width" id="reset-filters" onClick={this.resetFilters}>Reset</div>
            </div>
            <div className="ncss-col-sm-6 mb4-sm">
              <div className="ncss-btn-black ncss-brand pt2-sm pb2-sm u-uppercase u-full-width" id="submit-filters">Apply</div>
            </div>
          </div>
        </div>
        </div>
      </aside>
    )
  }
}
